import axios from "axios";

interface PaginationInfoT {
  totalPages: number;
  count: Number;
  pages: Number;
  next: String | null;
  prev: String | null;
}

interface PaginatedResponseT<T> {
  info: PaginationInfoT;
  results: Array<T>;
}

const httpClient = axios.create({
  baseURL: import.meta.env.VITE_API_URL,
});

function getCancelToken() {
  return axios.CancelToken.source();
}

export { httpClient, getCancelToken };
export type { PaginatedResponseT };
export type { CancelToken } from "axios";
