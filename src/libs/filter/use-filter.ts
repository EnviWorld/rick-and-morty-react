import React from "react";

interface FilterParamsT {
  name: string;
  status: string;
  species: string;
}

const useFilter = () => {
  const [filter, setFilter] = React.useState<FilterParamsT>({
    name: "",
    status: "",
    species: "",
  });

  const handleSearchChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const { name, value } = e.target;
      setFilter((filter) => ({ ...filter, [name]: value }));
    },
    []
  );

  return {
    filter,
    handleSearchChange,
  };
};

export { useFilter };
