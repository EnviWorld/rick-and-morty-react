import React from "react";

interface PaginatorT {
  totalPages: number;
  currentPage: number;
}

interface UsePaginatorParamsT {
  page: number;
}

const usePaginator = ({ page }: UsePaginatorParamsT) => {
  const [paginator, setPaginator] = React.useState<PaginatorT>({
    currentPage: page,
    totalPages: page,
  });

  const handleNextPageClick = React.useCallback(() => {
    setPaginator((paginator) =>
      paginator.totalPages && paginator.currentPage < paginator.totalPages
        ? { ...paginator, currentPage: paginator.currentPage + 1 }
        : paginator
    );
  }, []);

  const handlePrevPageClick = React.useCallback(() => {
    setPaginator((paginator) =>
      paginator.currentPage > 1
        ? { ...paginator, currentPage: paginator.currentPage - 1 }
        : paginator
    );
  }, []);

  return {
    paginator,
    setPaginator,
    handleNextPageClick,
    handlePrevPageClick,
  };
};

export { usePaginator };
