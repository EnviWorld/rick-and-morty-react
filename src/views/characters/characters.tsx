import React from "react";
import { CancelToken, getCancelToken } from "../../libs/http-client";
import { usePaginator } from "../../libs/paginator/use-paginator";
import { CharacterT } from "./models";
import { Paginator } from "../../components/paginator";
import { Filter } from "../../components/filter";
import { Spinner } from "../../components/spinner";
import { NoData } from "../../components/no-data";
import { ErrorMessage } from "../../components/error-message";
import * as characterService from "./character-services";
import { useFilter } from "../../libs/filter/use-filter";

interface ICharactersProps {
  page: number;
  onPageChange: (props: Pick<ICharactersProps, "page">) => void;
}

function useCharacters({ page }: Pick<ICharactersProps, "page">) {
  const { paginator, setPaginator, handleNextPageClick, handlePrevPageClick } =
    usePaginator({ page });
  const { filter, handleSearchChange } = useFilter();

  const [characters, setCharacters] = React.useState<CharacterT[]>([]);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isFailed, setIsFailed] = React.useState(false);

  const getCharacters = React.useCallback(
    (
      {
        page,
        name,
        status,
        species,
      }: {
        page: number;
        name?: string;
        status?: string;
        species?: string;
      },
      config: { cancelToken: CancelToken }
    ) => {
      setIsLoading(true);
      const params = {
        page: page,
        name: name,
        status: status,
        species: species,
      };

      characterService.getCharacters({ params, ...config }).then(
        (response) => {
          const { info, results } = response.data;

          setIsFailed(false);
          setIsLoading(false);
          setCharacters(
            results.map((character) => {
              return {
                ...character,
              };
            })
          );

          setPaginator({
            currentPage: page,
            totalPages: Number(info.pages),
          });
        },
        (error) => {
          setIsFailed(true);
          setIsLoading(false);
          setCharacters([]);
        }
      );
    },
    []
  );

  const onFocusFilter = React.useCallback(() => {
    setPaginator({ currentPage: 1, totalPages: paginator.totalPages });
  }, []);

  return {
    characters,
    isLoading,
    isFailed,
    paginator,
    filter,
    handleNextPageClick,
    handlePrevPageClick,
    handleSearchChange,
    getCharacters,
    onFocusFilter,
  };
}

function Characters({ page, onPageChange }: ICharactersProps) {
  const {
    characters,
    isLoading,
    isFailed,
    paginator,
    filter,
    handleNextPageClick,
    handlePrevPageClick,
    handleSearchChange,
    getCharacters,
    onFocusFilter,
  } = useCharacters({ page });

  React.useEffect(() => {
    onPageChange({ page: paginator.currentPage });
  }, [onPageChange, paginator.currentPage]);

  React.useEffect(() => {
    const source = getCancelToken();

    getCharacters(
      { page: paginator.currentPage, ...filter },
      { cancelToken: source.token }
    );

    return () => source.cancel();
  }, [paginator.currentPage, getCharacters, filter]);

  return (
    <div className="App">
      <h1 className="text-center mb-3">Characters</h1>
      <div className="container">
        <div className="row">
          {paginator.totalPages > 1 ? (
            <Paginator
              totalPages={paginator.totalPages}
              currentPage={paginator.currentPage}
              onNextPageClick={handleNextPageClick}
              onPreviousPageClick={handlePrevPageClick}
            />
          ) : null}

          <Filter
            onSearchChange={handleSearchChange}
            onFocusFilter={onFocusFilter}
          />
          {isLoading ? (
            <div className="row">
              <div className="col-2 offset-5">
                <Spinner />
              </div>
            </div>
          ) : isFailed ? (
            <ErrorMessage label="Error: Couldn't load characters" />
          ) : characters.length === 0 ? (
            <NoData label="There are no characters yet" />
          ) : (
            <div className="col-lg-12 col-12">
              <div className="row">
                {characters.map((character) => (
                  <Character
                    key={character.id.toString()}
                    id={character.id}
                    data={character}
                  ></Character>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

interface CharacterProps {
  id: Number;
  data: CharacterT;
}

import "./character.css";

function Character(props: CharacterProps) {
  const { name, status, species, image, gender, origin, created, location } =
    props.data;

  return (
    <div
      className={`col-4 mb-2 mt-2 d-flex flex-column justify-content-center`}
    >
      <div className=" card">
        <div
          className={`badge  ${
            status === "Alive"
              ? "bg-success"
              : status === "Dead"
              ? "bg-danger"
              : "bg-secondary"
          }`}
        >
          {status}
        </div>
        <div className="col-4 mt-1 offset-4">
          <img className={`img img-fluid`} src={image.toString()} alt="" />
        </div>
        <div className={`content`}>
          <div className="fs-5 fw-bold mb-4">{name}</div>
          <div className="">
            <div className="fs-6 fw-normal">Gender</div>
            <div className="fs-5">{gender}</div>
          </div>
          <div className="">
            <div className="fs-6 fw-normal">Species</div>
            <div className="fs-5">{species}</div>
          </div>
          <div className="">
            <div className="fs-6 fw-normal">Origin</div>
            <div className="fs-5">{origin.name}</div>
          </div>
          <div className="">
            <div className="fs-6 fw-normal">Last Location</div>
            <div className="fs-5">{location.name}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export { Characters };
export type { ICharactersProps };
