import React from "react";
import {
  Navigate,
  Route,
  Routes,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { Characters, ICharactersProps } from "./characters";

function CharactersRouter() {
  const [params] = useSearchParams();
  const page = Number(params.get("page") || 1);

  const navigate = useNavigate();

  const handlePageChange = React.useCallback<ICharactersProps["onPageChange"]>(
    ({ page }) => {
      navigate(`?page=${page}`);
    },
    [navigate]
  );

  return (
    <Routes>
      <Route
        index
        element={<Characters page={page} onPageChange={handlePageChange} />}
      />
      <Route path="*" element={<Navigate to="" />} />
    </Routes>
  );
}

export { CharactersRouter };
