import { CharacterT } from "./models";
import {
  httpClient,
  CancelToken,
  PaginatedResponseT,
} from "../../libs/http-client";

type CharacterResponseT = PaginatedResponseT<CharacterT>;

interface GetCharactersParamsT {
  params: { page?: number; name?: string; status?: string; species?: string };
  cancelToken: CancelToken;
}

const BASE_URL = import.meta.env.VITE_API_URL + "character";

const getCharacters = ({ params, ...config }: GetCharactersParamsT) => {
  return httpClient.get<CharacterResponseT>(BASE_URL, {
    params,
    ...config,
  });
};

export { getCharacters };
