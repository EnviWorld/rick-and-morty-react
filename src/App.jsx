import { Navigate, Route, Routes } from "react-router-dom";
import { CharactersRouter } from "./views/characters/characters-router";

function App() {
  return (
    <div className="app-container">
      <Routes>
        <Route path="characters/*" element={<CharactersRouter />} />
        <Route path="" element={<Navigate to="characters" />} />
      </Routes>
    </div>
  );
}

export { App };
