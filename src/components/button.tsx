import React from 'react'

type ButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
  disabled?: boolean
}

const Button = ({ children, ...rest }: ButtonProps) => (
  <button {...rest} className="btn btn-success col-3">
    {children}
  </button>
)

export { Button }
