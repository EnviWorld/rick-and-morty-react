import { Button } from "./button";

interface PaginatorProps {
  totalPages: number;
  currentPage: number;
  onNextPageClick(): void;
  onPreviousPageClick(): void;
}

const Paginator = (props: PaginatorProps) => {
  return (
    <div className="row d-flex flex-sm-row flex-column align-items-center justify-content-center gap-4 mb-5">
      <Button
        onClick={props.onPreviousPageClick}
        disabled={props.currentPage === 1}
      >
        Previous
      </Button>
      <label className="col-3 d-flex justify-content-center">
        {" "}
        Page {props.currentPage} of {props.totalPages}{" "}
      </label>
      <Button
        onClick={props.onNextPageClick}
        disabled={props.currentPage === props.totalPages}
      >
        Next
      </Button>
    </div>
  );
};

export { Paginator };
