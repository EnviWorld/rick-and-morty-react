interface FilterPropsT {
  onSearchChange(event): void;
  onFocusFilter(): void;
}

const Filter = (props: FilterPropsT) => {
  return (
    <form className="row d-flex flex-sm-row flex-column align-items-center justify-content-center gap-4 mb-5">
      <input
        type="text"
        onChange={props.onSearchChange}
        onFocus={props.onFocusFilter}
        name="name"
        placeholder="Name"
        className={`input col-3`}
      ></input>

      <select
        onChange={props.onSearchChange}
        name="status"
        className="col-3"
        onFocus={props.onFocusFilter}
      >
        <option value="">Select status</option>
        <option value="alive">Alive</option>
        <option value="dead">Dead</option>
        <option value="unknown">Unknown</option>
      </select>
      <input
        type="text"
        onChange={props.onSearchChange}
        onFocus={props.onFocusFilter}
        name="species"
        placeholder="Species"
        className={`input col-3`}
      />
    </form>
  );
};

export { Filter };
